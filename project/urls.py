"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include

from app_base import views as base_views
from app_course import views as course_views
from app_course_file import views as course_file_views
from app_course_test import views as course_test_views
from app_course_topic import views as course_topic_views
from app_semester import views as semester_views
from app_student_group import views as student_group_views
from app_student_status import views as student_status_views
from app_user import views as user_views

base_namespace = ([
    path('', base_views.IndexPage.as_view(), name='index'),
], 'base')

course_namespace = ([
    path('admin_list/', course_views.admin_list, name='admin_list'),
    path('admin_create/', course_views.admin_form, name='admin_create'),
    path('admin_edit/<int:course_pk>/', course_views.admin_form, name='admin_edit'),
    path('admin_delete/<int:course_pk>/', course_views.admin_delete, name='admin_delete'),
    path('admin_student/', course_views.admin_students, name='admin_student'),  # Don't use this URL
    path('admin_student/<int:semester_pk>/', course_views.admin_students, name='admin_student'),
    path('admin_student/<int:semester_pk>/<int:course_pk>/', course_views.admin_students, name='admin_student'),
    path('teacher_list/', course_views.teacher_list, name='teacher_list'),
    path('teacher_page/<int:course_pk>/', course_views.teacher_page, name='teacher_page'),
    path('student_list/', course_views.student_list, name='student_list'),
    path('student_page/<int:course_pk>/', course_views.student_page, name='student_page'),
], 'course')

course_file_namespace = ([
    path('create/<int:pk>/', course_file_views.Create.as_view(), name='create'),
    path('edit/<int:pk>/', course_file_views.Update.as_view(), name='edit'),
    path('delete/<int:pk>/', course_file_views.Delete.as_view(), name='delete'),
], 'course_file')

course_test_namespace = ([
    path('test/<int:test_pk>/', course_test_views.test_page, name='test_page'),
], 'course_test')

course_topic_namespace = ([
    path('create/<int:course_pk>/', course_topic_views.topic_create, name='create'),
    path('edit/<int:topic_pk>/', course_topic_views.topic_form, name='edit'),
    path('page/<int:topic_pk>/', course_topic_views.topic_page, name='topic_page'),
    path('delete/<int:pk>/', course_topic_views.Delete.as_view(), name='delete'),
], 'course_topic')

semester_namespace = ([
    path('list/', semester_views.semester_list, name='list'),
    path('create/', semester_views.semester_form, name='create'),
    path('edit/<int:semester_pk>/', semester_views.semester_form, name='edit'),
    path('delete/<int:semester_pk>/', semester_views.semester_delete, name='delete'),
], 'semester')

student_group_namespace = ([
    path('list/', student_group_views.group_list, name='list'),
    path('create/', student_group_views.group_form, name='create'),
    path('edit/<int:group_pk>/', student_group_views.group_form, name='edit'),
    path('delete/<int:group_pk>/', student_group_views.group_delete, name='delete'),
], 'student_group')

student_status_namespace = ([
    path('create/<int:pk>/', student_status_views.Create.as_view(), name='create'),
    path('edit/<int:pk>/', student_status_views.Update.as_view(), name='edit'),
    path('delete/<int:pk>/', student_status_views.Delete.as_view(), name='delete'),
], 'student_status')

user_namespace = ([
    path('login/', user_views.LoginPage.as_view(), name='login'),
    path('logout/', user_views.LogoutPage.as_view(), name='logout'),
    path('password/', user_views.PasswordChangePage.as_view(), name='change_password'),
    path('password/<int:user_id>/', user_views.SetPasswordPage.as_view(), name='set_password'),
    path('list/', user_views.List.as_view(), name='list'),
    path('create/', user_views.Create.as_view(), name='create'),
    path('edit/<int:pk>/', user_views.Update.as_view(), name='edit'),
    path('delete/<int:pk>/', user_views.Delete.as_view(), name='delete'),
], 'user')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(base_namespace)),
    path('course/', include(course_namespace)),
    path('course_file/', include(course_file_namespace)),
    path('course_test/', include(course_test_namespace)),
    path('course_topic/', include(course_topic_namespace)),
    path('semester/', include(semester_namespace)),
    path('student_group/', include(student_group_namespace)),
    path('student_status/', include(student_status_namespace)),
    path('user/', include(user_namespace)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))

    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
