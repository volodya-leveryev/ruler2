from django.apps import AppConfig


class StudentGroupConfig(AppConfig):
    name = 'app_student_group'
