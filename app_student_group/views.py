from django.contrib.auth.decorators import user_passes_test
from django.forms import ModelForm
from django.shortcuts import render, get_object_or_404, redirect

from app_semester.models import Semester
from app_student_group.models import StudentGroup


class GroupForm(ModelForm):
    class Meta:
        model = StudentGroup
        fields = '__all__'


@user_passes_test(lambda u: u.is_superuser)
def group_list(request):
    return render(request, 'student_group/list.html', {
        'current_list': StudentGroup.objects.filter(is_active=True),
        'archive_list': StudentGroup.objects.filter(is_active=False)
    })


@user_passes_test(lambda u: u.is_superuser)
def group_form(request, group_pk=None):
    group = get_object_or_404(StudentGroup, pk=group_pk) if group_pk else None
    if request.method == 'POST':
        form = GroupForm(request.POST, instance=group)
        if form.is_valid():
            form.save()
            return redirect('student_group::list')
    else:
        form = GroupForm(instance=group)
    context = {'form': form}
    if group_pk:
        context['semesters'] = Semester.objects.filter(student_group=group)
    return render(request, 'student_group/form.html', context)


@user_passes_test(lambda u: u.is_superuser)
def group_delete(request, group_pk):
    group = get_object_or_404(StudentGroup, pk=group_pk)
    if request.method == 'POST':
        group.delete()
        return redirect('student_group::list')
    return render(request, 'student_group/confirm_delete.html', {'group': group})
