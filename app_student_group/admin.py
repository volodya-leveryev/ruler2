from django.contrib import admin

from app_student_group.models import StudentGroup

admin.site.register(StudentGroup)
