from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsAdmin
from app_student_group.models import StudentGroup


class TestStudentGroupModel(TestCase):

    def test_str(self):
        obj = mommy.make(StudentGroup)
        self.assertEqual(obj.name, str(obj))


class TestAdminStudentGroupViews(TestViewsAdmin):

    def setUp(self):
        self.admin = mommy.prepare(User, username='admin', is_superuser=True, is_staff=False)
        self.admin.set_password('password')
        self.admin.save()

        self.teacher = mommy.prepare(User, username='teacher', is_superuser=False, is_staff=True)
        self.teacher.set_password('password')
        self.teacher.save()

        self.student = mommy.prepare(User, username='student', is_superuser=False, is_staff=False)
        self.student.set_password('password')
        self.student.save()

        self.student_group = mommy.make(StudentGroup, name='group')
        self.group_list_url = reverse('student_group:list')
        self.group_edit_url = reverse('student_group:edit', args=[self.student_group.id])

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.group_list_url,
            self.group_edit_url,
        ])

    def test_admin_can_view_student_group_list(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.group_list_url)
        self.assertContains(r, self.student_group.name)

    def test_admin_can_view_student_group_update(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.group_edit_url)
        self.assertContains(r, self.student_group.name)
