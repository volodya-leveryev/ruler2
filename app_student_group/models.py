import datetime

from django.db import models
from django.db.models.query import RawQuerySet


class StudentGroup(models.Model):
    BACHELOR = 'B'
    SPECIALIST = 'S'
    MASTER = 'M'
    DEGREE = (
        (BACHELOR, 'бакалавриат'),
        (MASTER, 'магистратура'),
        (SPECIALIST, 'специалитет'),
    )

    FULL_TIME = 'F'
    PART_TIME = 'P'
    FORM = (
        (FULL_TIME, 'очная'),
        (PART_TIME, 'заочная')
    )

    name = models.CharField('наименование', max_length=30)
    year = models.IntegerField('год поступления')
    degree = models.CharField('квалификация', max_length=1, choices=DEGREE)
    form = models.CharField('форма обучения', max_length=1, choices=FORM)
    is_active = models.BooleanField('в процессе обучения', default=False)

    class Meta:
        verbose_name = 'учебная группа'
        verbose_name_plural = 'учебные группы'
        ordering = ['degree', 'year', 'name']

    def __str__(self):
        return self.name

    def get_students(self, date: datetime.date) -> RawQuerySet:
        """Студенты, которые учатся в этом семестре в этой группе"""
        from app_user.models import User
        return User.objects.raw("""
            SELECT
                t_user.id, t_user.username, t_user.first_name, t_user.last_name
            FROM (
                SELECT student_id, MAX(moment) AS moment 
                FROM app_student_status_studentstatus 
                GROUP BY student_id 
                HAVING DATE(moment) <= '%s'
                ) AS t_base 
            LEFT JOIN 
                app_student_status_studentstatus AS t_status 
                ON t_base.student_id = t_status.student_id AND t_base.moment = t_status.moment
            LEFT JOIN 
                app_user_user AS t_user 
                ON t_base.student_id = t_user.id
            WHERE
              t_status.status = 'E' and t_status.student_group_id = '%d'
        """ % (date.isoformat(), self.id))
