from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import user_passes_test, login_required
from django.contrib.auth.views import redirect_to_login
from django.forms import ModelForm
from django.shortcuts import get_object_or_404, render, redirect

from app_course.models import Course, CourseElement
from app_course_test.models import Test
from app_course_topic.models import Topic
from app_semester.models import Semester


class CourseForm(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'


def teacher_required(cls, key='pk'):
    """
    Порождает декоратор для защиты CBV или view-функций от других преподавателей
    :param cls: класс Course или класс-потомок CourseElement
    :param key: имя параметра запроса хранящего идентификатор объекта класса cls
    :return: декоратор
    """
    def decorator(view_func):
        def wrapper(request, *args, **kwargs):
            if request.user.is_staff:
                obj = get_object_or_404(cls, pk=request.resolver_match.kwargs[key])
                if isinstance(obj, Course):
                    if request.user == obj.teacher:
                        return view_func(request, *args, **kwargs)
                elif isinstance(obj, CourseElement):
                    if request.user == obj.course.teacher:
                        return view_func(request, *args, **kwargs)
            return redirect_to_login(request.path, settings.LOGIN_URL, REDIRECT_FIELD_NAME)
        return wrapper
    return decorator


@user_passes_test(lambda u: u.is_superuser)
def admin_list(request):
    return render(request, 'course/list.html', {'course_list': Course.objects.all()})


@user_passes_test(lambda u: u.is_superuser)
def admin_form(request, course_pk=None):
    course = get_object_or_404(Course, pk=course_pk) if course_pk else None
    if request.method == 'POST':
        form = CourseForm(request.POST, instance=course)
        if form.is_valid():
            form.save()
            return redirect('course:admin_list')
    else:
        form = CourseForm(instance=course)
    context = {'form': form}
    if course_pk:
        context['students_available'] = list(course.semester.get_students())
        context['students_selected'] = list(course.students.all())
    return render(request, 'course/form.html', context)


@user_passes_test(lambda u: u.is_superuser)
def admin_delete(request, course_pk=None):
    course = get_object_or_404(Course, pk=course_pk)
    if request.method == 'POST':
        course.delete()
        return redirect('course:admin_list')
    return render(request, 'course/confirm_delete.html', {'course': course})


@user_passes_test(lambda u: u.is_superuser)
def admin_students(request, semester_pk, course_pk=None):
    semester = get_object_or_404(Semester, pk=semester_pk)
    context = {'students_available': list(semester.get_students())}
    if course_pk:
        course = get_object_or_404(Course, pk=course_pk)
        context['students_selected'] = list(course.students.all())
    return render(request, 'course/students.html', context)


@user_passes_test(lambda u: u.is_staff)
def teacher_list(request):
    return render(request, 'course/list_teacher.html', {'teaching_load': request.user.teaching_load.all()})


@user_passes_test(lambda u: u.is_staff)
def teacher_page(request, course_pk):
    course = get_object_or_404(Course, pk=course_pk)
    elements = list(course.topic_set.all()) + list(course.file_set.all())
    return render(request, 'course/detail_teacher.html', {
        'course': course,
        'elements': sorted(elements, key=lambda e: e.order)
    })


@login_required
def student_list(request):
    return render(request, 'course/course_list.html', {'studied_courses': request.user.studied_courses.all()})


@login_required
def student_page(request, course_pk):
    course = get_object_or_404(Course, pk=course_pk)
    topics = list(Topic.objects.filter(course=course))
    tests = list(Test.objects.filter(course=course, user=request.user))
    return render(request, 'course/course_detail.html', {
        'elements': sorted(topics + tests, key=lambda elem: elem.order)
    })
