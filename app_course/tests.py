import datetime
import os

from django.conf import settings
from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsAdmin
from app_course.models import Course
from app_course_file.models import File
from app_course_topic.models import Topic
from app_semester.models import Semester
from app_student_group.models import StudentGroup
from app_student_status.models import StudentStatus


class TestCourseModel(TestCase):

    def test_str(self):
        obj = mommy.make(Course)
        self.assertEqual(obj.name, str(obj))

    def test_folder(self):
        """ Каталог для хранения файлов курса """
        obj = mommy.make(Course)
        correct = os.path.join(settings.MEDIA_ROOT, 'courses', str(obj.pk))
        self.assertEqual(correct, obj.get_folder())


class TestAdminCourseViews(TestViewsAdmin):

    def setUp(self):
        self.admin = mommy.prepare(User, username='admin', is_superuser=True, is_staff=False)
        self.admin.set_password('password')
        self.admin.save()

        self.student = mommy.prepare(User, username='student', is_superuser=False, is_staff=False)
        self.student.set_password('password')
        self.student.save()

        moment = datetime.datetime(2018, 9, 1)
        group = mommy.make(StudentGroup, name='group')
        semester = mommy.make(Semester, student_group=group, begin_study=moment.date())
        mommy.make(StudentStatus, moment=moment, student=self.student, student_group=group, status='E')
        self.course = mommy.make(Course, name='course', semester=semester)
        self.course.students.add(self.student)

        self.course_list_url = reverse('course:admin_list')
        self.course_edit_url = reverse('course:admin_edit', args=[self.course.id])
        self.course_students_url = reverse('course:admin_student', args=[self.course.id, self.course.id])

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.course_list_url,
            self.course_edit_url,
            self.course_students_url,
        ])

    def test_admin_can_view_course_list(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.course_list_url)
        self.assertContains(r, self.course.name)

    def test_admin_can_view_course_edit(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.course_edit_url)
        self.assertContains(r, self.course.name)

    def test_admin_can_view_course_students(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.course_students_url)
        self.assertContains(r, self.student)


class TestTeacherCourseViews(TestCase):

    def setUp(self):
        user1 = mommy.prepare(User, username='user1', is_staff=True)
        user1.set_password('password')
        user1.save()

        self.course1 = mommy.make(Course, name='course1', teacher=user1)
        self.topic1 = mommy.make(Topic, name='topic1', course=self.course1)
        self.file1 = mommy.make(File, name='file1', course=self.course1)

        user2 = mommy.prepare(User, username='user2', is_staff=True)
        user2.set_password('password')
        user2.save()

        self.course2 = mommy.make(Course, name='course2', teacher=user2)
        self.topic2 = mommy.make(Topic, name='topic2', course=self.course2)
        self.file2 = mommy.make(File, name='file2', course=self.course2)

        self.client.login(username='user1', password='password')

    def test_course_list(self):
        r = self.client.get(reverse('course:teacher_list'))
        self.assertContains(r, self.course1.name)

    def test_course_detail(self):
        r = self.client.get(reverse('course:teacher_page', args=[self.course1.id]))
        self.assertContains(r, self.course1.name)
        self.assertContains(r, self.topic1.name)
        self.assertContains(r, self.file1.name)
        self.assertNotContains(r, self.course2.name)
        self.assertNotContains(r, self.topic2.name)
        self.assertNotContains(r, self.file2.name)
        self.assertContains(r, '</table>', count=1)


class TestStudentCourseViews(TestCase):

    def setUp(self):
        user = mommy.prepare(User, username='user')
        user.set_password('password')
        user.save()

        self.course = mommy.make(Course, name='course')

    def test_course_list(self):
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course:student_list'))
        self.assertContains(r, self.course.name)

    def test_course_page(self):
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course:student_page', args=[self.course.id]))
        self.assertContains(r, self.course.name)
