import os

from django.conf import settings
from django.db import models


# TODO: Преподаватель должен иметь возможность добавить файлы к курсу
class Course(models.Model):
    MANDATORY = 1
    OPTIONAL = 2
    COURSE_TYPE = (
        (MANDATORY, 'обязательный'),
        (OPTIONAL, 'курс по выбору'),
    )

    name = models.CharField('наименование', max_length=150)
    semester = models.ForeignKey('app_semester.Semester', on_delete=models.DO_NOTHING, verbose_name='семестр')
    teacher = models.ForeignKey('app_user.User', on_delete=models.DO_NOTHING, related_name='teaching_load',
                                verbose_name='преподаватель')
    course_type = models.IntegerField('тип курса', choices=COURSE_TYPE)
    students = models.ManyToManyField('app_user.User', blank=True, related_name='studied_courses',
                                      verbose_name='студенты')
    lecture_hours = models.IntegerField('часы лекций', blank=True, default=0)
    lab_work_hours = models.IntegerField('часы лаб. работ', blank=True, default=0)
    practice_hours = models.IntegerField('часы практ. работ', blank=True, default=0)
    student_hours = models.IntegerField('часы СРС', blank=True, default=0)
    control_hours = models.IntegerField('часы КСР', blank=True, default=0)
    total_hours = models.IntegerField('часов всего', blank=True, default=0)

    class Meta:
        verbose_name = 'дисциплина'
        verbose_name_plural = 'дисциплины'

    def __str__(self):
        return self.name

    def get_folder(self):
        return os.path.join(settings.MEDIA_ROOT, 'courses', str(self.pk))


class CourseElement(models.Model):
    kind = 'element'

    course = models.ForeignKey('app_course.Course', on_delete=models.CASCADE, verbose_name='курс обучения')
    order = models.IntegerField('порядковый номер')
    name = models.CharField('наименование', max_length=150, unique=True)

    class Meta:
        abstract = True
        ordering = ['order']

    def __str__(self):
        return self.name
