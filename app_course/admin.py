from django.contrib import admin

from app_course.models import Course

admin.site.register(Course)
