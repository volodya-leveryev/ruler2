from django.apps import AppConfig


class CourseFileConfig(AppConfig):
    name = 'app_course_file'
