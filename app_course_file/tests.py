import os

from django.conf import settings
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsTeacher
from app_course.models import Course


class TestTeacherFileViews(TestViewsTeacher):

    def setUp(self):
        course_owner = mommy.prepare(User, username='user', is_staff=True)
        course_owner.set_password('password')
        course_owner.save()

        self.course = mommy.make(Course, name='course', teacher=course_owner)
        # self.file = mommy.make(File, name='file', course=self.course)

        self.course_url = reverse('course:teacher_page', args=[self.course.id])
        self.file_create_url = reverse('course_file:create', args=[self.course.id])
        # self.file_edit_url = reverse('course_file:edit', args=[self.file.id])
        # self.file_delete_url = reverse('course_file:delete', args=[self.file.id])

        self.attachment = open(os.path.join(settings.BASE_DIR, 'manage.py'))
        self.file_data = {
            'course': self.course.id,
            'order': 1,
            'name': 'name',
            'file': self.attachment,
        }

        # folder =

    def tearDown(self):
        self.attachment.close()

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.file_create_url,
            # self.file_edit_url,
            # self.file_delete_url,
        ])

    def test_file_list(self):
        self.client.login(username='user', password='password')
        r = self.client.get(self.file_create_url)
        self.assertContains(r, 'Создание файла')
        r = self.client.post(self.file_create_url, self.file_data)
        self.assertRedirects(r, self.course_url)

    # def test_file_create(self):
    #     self.client.login(username='user', password='password')
    #     r = self.client.get(self.file_create_url)
    #     self.assertContains(r, 'Создание файла')
    #     r = self.client.post(self.file_create_url, self.file_data)
    #     self.assertRedirects(r, self.course_url)

    # def test_file_edit(self):
    #     self.client.login(username='user', password='password')
    #     r = self.client.get(self.file_edit_url)
    #     self.assertContains(r, self.file.name)
    #     r = self.client.post(self.file_edit_url, self.file_data)
    #     self.assertRedirects(r, self.course_url)

    # def test_file_delete(self):
    #     self.client.login(username='user', password='password')
    #     r = self.client.get(self.file_delete_url)
    #     self.assertContains(r, self.file.name)
    #     r = self.client.post(self.file_delete_url, {})
    #     self.assertRedirects(r, self.course_url)
