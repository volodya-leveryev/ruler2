from django.db import models

from app_course.models import CourseElement


class File(CourseElement):
    kind = 'file'

    file = models.FileField('файл')

    class Meta:
        verbose_name = 'загруженный файл'
        verbose_name_plural = 'загруженные файлы'
