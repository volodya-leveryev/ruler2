from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, DeleteView, UpdateView

from app_course.models import Course
from app_course.views import teacher_required
from app_course_file.models import File


@method_decorator(teacher_required(Course), name='dispatch')
class Create(CreateView):
    fields = '__all__'
    model = File
    template_name = 'course_file/file_form.html'

    def get_initial(self):
        initial = super().get_initial()
        initial['course'] = get_object_or_404(Course, pk=self.kwargs['pk'])
        return initial

    def form_valid(self, form):
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('course:teacher_page', args=[self.object.course.id])


@method_decorator(teacher_required(File), name='dispatch')
class Update(UpdateView):
    fields = '__all__'
    model = File
    template_name = 'course_file/file_form.html'

    def get_success_url(self):
        return reverse_lazy('course:teacher_page', args=[self.object.course.id])


@method_decorator(teacher_required(File), name='dispatch')
class Delete(DeleteView):
    model = File
    template_name = 'course_file/confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('course:teacher_page', args=[self.object.course.id])
