from django.db import models

from app_course.models import CourseElement


class Topic(CourseElement):
    kind = 'topic'

    text = models.TextField('текст', blank=True)

    class Meta:
        verbose_name = 'тема'
        verbose_name_plural = 'темы'
