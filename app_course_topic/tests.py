from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsTeacher
from app_course.models import Course
from app_course_topic.models import Topic


class TestTopicModel(TestCase):

    def test_str(self):
        obj = mommy.make(Topic)
        self.assertEqual(obj.name, str(obj))


class TestTeacherTopicViews(TestViewsTeacher):

    def setUp(self):
        course_owner = mommy.prepare(User, username='user', is_staff=True)
        course_owner.set_password('password')
        course_owner.save()

        self.course = mommy.make(Course, name='course', teacher=course_owner)
        self.topic = mommy.make(Topic, name='topic', course=self.course)

        self.topic_course_url = reverse('course:teacher_page', args=[self.course.id])
        self.topic_create_url = reverse('course_topic:create', kwargs={'course_pk': self.course.id})
        self.topic_edit_url = reverse('course_topic:edit', kwargs={'topic_pk': self.topic.id})
        self.topic_delete_url = reverse('course_topic:delete', args=[self.topic.id])

        self.topic_data = {
            'course': self.course.id,
            'order': 1,
            'name': 'name',
            'text': 'text',
        }

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.topic_create_url,
            self.topic_edit_url,
            self.topic_delete_url,
        ])

    def test_topic_create(self):
        self.client.login(username='user', password='password')
        r = self.client.get(self.topic_create_url)
        self.assertContains(r, 'Создание темы')
        r = self.client.post(self.topic_create_url, self.topic_data)
        self.assertRedirects(r, self.topic_course_url)

    def test_topic_edit(self):
        self.client.login(username='user', password='password')
        r = self.client.get(self.topic_edit_url)
        self.assertContains(r, self.topic.name)
        r = self.client.post(self.topic_edit_url, self.topic_data)
        self.assertRedirects(r, self.topic_course_url)

    def test_topic_delete(self):
        self.client.login(username='user', password='password')
        r = self.client.get(self.topic_delete_url)
        self.assertContains(r, self.topic.name)
        r = self.client.post(self.topic_delete_url, {})
        self.assertRedirects(r, self.topic_course_url)


class TestStudentTopicViews(TestCase):

    def setUp(self):
        user = mommy.prepare(User, username='user')
        user.set_password('password')
        user.save()

        self.course = mommy.make(Course, name='course')
        self.topic = mommy.make(Topic, course=self.course, order=1, name='topic')

    def test_course_page(self):
        """ Студент на странице курса должен видеть названия тем """
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course:student_page', args=[self.course.id]))
        self.assertContains(r, self.topic.name)

    def test_topic_page(self):
        """ Студент на странице темы курса должен видеть названия темы """
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course_topic:page', args=[self.topic.id]))
        self.assertContains(r, self.topic.name)
