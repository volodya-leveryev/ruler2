from django.contrib import admin

from app_course_topic.models import Topic


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('order', 'name', 'course')
    list_filter = ('course',)
