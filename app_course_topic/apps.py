from django.apps import AppConfig


class CourseTopicConfig(AppConfig):
    name = 'app_course_topic'
