from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import DeleteView
from markdown import markdown

from app_course.models import Course
from app_course.views import teacher_required
from app_course_topic.models import Topic


class TopicForm(ModelForm):
    class Meta:
        model = Topic
        fields = '__all__'


# @method_decorator(teacher_required(Course), name='dispatch')
# class Create(CreateView):
#     fields = '__all__'
#     model = models.Topic
#     template_name = 'ruler/course_topic/topic_form.html'
#
#     def get_initial(self):
#         initial = super().get_initial()
#         initial['course'] = get_object_or_404(Course, pk=self.kwargs['pk'])
#         return initial
#
#     def get_success_url(self):
#         # return reverse_lazy('course:teacher_page', kwargs={'pk': self.object.course.id})
#         return reverse_lazy('course:teacher_page', args=[self.object.course.id])


@teacher_required(Course, key='course_pk')
def topic_create(request, course_pk):
    initials = {'course': get_object_or_404(Course, pk=course_pk)}
    if request.method == 'POST':
        form = TopicForm(request.POST, initial=initials)
        if form.is_valid():
            form.save()
            return redirect('course:teacher_page', course_pk=form.instance.course.id)
    else:
        form = TopicForm(initial=initials)
    return render(request, 'course_topic/topic_form.html', {'form': form})


# @method_decorator(teacher_required(models.Topic), name='dispatch')
# class Update(UpdateView):
#     fields = '__all__'
#     model = models.Topic
#     template_name = 'ruler/course_topic/topic_form.html'
#
#     def get_success_url(self):
#         # return reverse_lazy('course:teacher_page', kwargs={'pk': self.object.course.id})
#         return reverse_lazy('course:teacher_page', args=[self.object.course.id])


@teacher_required(Course, key='topic_pk')
def topic_form(request, topic_pk):
    topic = get_object_or_404(Topic, pk=topic_pk)
    if request.method == 'POST':
        form = TopicForm(request.POST, instance=topic)
        if form.is_valid():
            form.save()
            return redirect('course:teacher_page', course_pk=form.instance.course.id)
    else:
        form = TopicForm(instance=topic)
    return render(request, 'course_topic/topic_form.html', {'form': form})


@method_decorator(teacher_required(Topic), name='dispatch')
class Delete(DeleteView):
    model = Topic
    template_name = 'course_topic/confirm_delete.html'

    def get_success_url(self):
        # return reverse_lazy('course:teacher_page', kwargs={'pk': self.object.course.id})
        return reverse_lazy('course:teacher_page', args=[self.object.course.id])


@login_required
def topic_page(request, topic_pk):
    topic = get_object_or_404(Topic, pk=topic_pk)
    course = topic.course
    text = topic.text.replace('%~', '/media/courses/%d/%d' % (course.id, topic.id))
    return render(request, 'course_topic/topic_detail.html', {
        'topic': topic,
        'course': course,
        'markdown': markdown(text, extensions={'markdown.extensions.attr_list'})
    })
