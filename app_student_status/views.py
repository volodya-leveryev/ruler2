from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import CreateView, UpdateView, DeleteView

from app_user.models import User
from app_student_status import models


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Create(CreateView):
    fields = '__all__'
    model = models.StudentStatus
    template_name = 'student_status/form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['student'] = get_object_or_404(User, pk=self.kwargs['pk'])
        return context

    def form_valid(self, form):
        form.instance.student = get_object_or_404(User, pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy('user:edit', kwargs={'pk': self.object.student.id})


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Update(UpdateView):
    fields = '__all__'
    model = models.StudentStatus
    template_name = 'student_status/form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['student'] = self.object.student
        return context

    def get_success_url(self):
        return reverse_lazy('user:edit', kwargs={'pk': self.object.student.id})


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Delete(DeleteView):
    model = models.StudentStatus
    template_name = 'student_status/confirm_delete.html'

    def get_success_url(self):
        return reverse_lazy('user:edit', kwargs={'pk': self.object.student.id})
