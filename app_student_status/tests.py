from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsAdmin
from app_student_status.models import StudentStatus


class TestStudentStatusModel(TestCase):

    def test_str(self):
        obj = mommy.make(StudentStatus)
        correct = '%s: %s %s' % (obj.moment.strftime('%Y.%m.%d %H:%M'), obj.student, obj.get_status_display())
        self.assertEqual(correct, str(obj))


class TestAdminStudentStatusViews(TestViewsAdmin):

    def setUp(self):
        self.admin = mommy.prepare(User, username='admin', is_superuser=True, is_staff=False)
        self.admin.set_password('password')
        self.admin.save()

        self.student = mommy.prepare(User, username='student', is_superuser=False, is_staff=False)
        self.student.set_password('password')
        self.student.save()

        self.status = mommy.make(StudentStatus, student=self.student)

        self.login_url = reverse('user:login')
        self.user_edit_url = reverse('user:edit', args=[self.student.id])
        self.status_create_url = reverse('student_status:create', args=[self.student.id])
        self.status_edit_url = reverse('student_status:edit', args=[self.status.id])
        self.status_delete_url = reverse('student_status:delete', args=[self.status.id])
        self.status_data = {
            'student': self.student.id,
            'moment': self.status.moment.strftime('%d.%m.%Y'),
            'status': 'E',
        }

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.user_edit_url,
            self.status_create_url,
            self.status_edit_url,
            self.status_delete_url,
        ])

    def test_admin_can_create_student_status(self):
        self.client.login(username=self.admin.username, password='password')

        r = self.client.get(self.status_create_url)
        self.assertContains(r, str(self.status.student))

        r = self.client.post(self.status_create_url, self.status_data)
        self.assertRedirects(r, self.user_edit_url)

    def test_admin_can_edit_student_status(self):
        self.client.login(username=self.admin.username, password='password')

        r = self.client.get(self.status_edit_url)
        self.assertContains(r, str(self.status.student))

        r = self.client.post(self.status_edit_url, self.status_data)
        self.assertRedirects(r, self.user_edit_url)

    def test_admin_can_delete_student_status(self):
        self.client.login(username=self.admin.username, password='password')

        r = self.client.get(self.status_delete_url)
        self.assertContains(r, str(self.status.student))

        r = self.client.post(self.status_delete_url, self.status_data)
        self.assertRedirects(r, self.user_edit_url)
