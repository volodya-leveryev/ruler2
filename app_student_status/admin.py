from django.contrib import admin

from app_student_status.models import StudentStatus

admin.site.register(StudentStatus)
