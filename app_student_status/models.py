from django.db import models


# TODO: Администратор должен иметь возможность изменять статус студентов
# - на странице учебной группы можно
#   - видеть список зачисленных студентов
#   - зачислить студента
#   - отчислить студента
class StudentStatus(models.Model):
    """
    В конкретный момент времени у студента может быть только один статус.
    Поля app_student_group и id_number следует проставлять только для статуса ENROLLED.
    """
    ENROLLED = 'E'
    EXCLUDED = 'X'
    ACADEMIC_LEAVE = 'A'
    TRANSFERRED = 'T'
    GRADUATED = 'G'
    STATUSES = (
        (ENROLLED, 'зачислен'),
        (EXCLUDED, 'отчислен'),
        (ACADEMIC_LEAVE, 'в академическом отпуске'),
        (TRANSFERRED, 'переведен'),
        (GRADUATED, 'выпущен'),
    )

    student = models.ForeignKey('app_user.User', on_delete=models.CASCADE, verbose_name='студент')
    moment = models.DateField('дата изменения статуса')
    status = models.CharField('статус', max_length=1, choices=STATUSES)
    student_group = models.ForeignKey('app_student_group.StudentGroup', on_delete=models.SET_NULL, blank=True, null=True,
                               verbose_name='учебная группа')
    id_number = models.CharField('номер зачётной книжки', max_length=10, blank=True)
    comment = models.TextField('комментарий', blank=True)

    class Meta:
        verbose_name = 'статус студента'
        verbose_name_plural = 'статусы студентов'
        ordering = ['moment']

    def __str__(self):
        return '%s: %s %s' % (self.moment.strftime('%Y.%m.%d %H:%M'), self.student, self.get_status_display())
