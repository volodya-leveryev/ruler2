from django.apps import AppConfig


class StudentStatusConfig(AppConfig):
    name = 'app_student_status'
