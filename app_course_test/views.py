from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404

from app_course_test.models import Test


@login_required
def test_page(request, test_pk):
    test = get_object_or_404(Test, pk=test_pk)
    return render(request, 'course_test/test_detail.html', {'test': test, 'course': test.course})
