from django.db import models

from app_course.models import CourseElement


class Test(CourseElement):
    kind = 'test'

    user = models.ForeignKey('app_user.User', on_delete=models.CASCADE, verbose_name='пользователь')

    class Meta:
        verbose_name = 'тест'
        verbose_name_plural = 'тесты'


class TestQuestion(models.Model):
    test = models.ForeignKey('app_course_test.Test', on_delete=models.CASCADE, verbose_name='тест')
    code = models.CharField('код', max_length=10, default='')
    text = models.TextField('текст вопроса')
    context = models.TextField('сериализованный контекст', blank=True)
    answer = models.TextField('сериализованный ответ', blank=True)
    checker = models.TextField('скрипт проверки', blank=True)

    class Meta:
        verbose_name = 'вопрос теста'
        verbose_name_plural = 'вопросы тестов'
        ordering = ['code']
