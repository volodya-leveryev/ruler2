from django.apps import AppConfig


class CourseTestConfig(AppConfig):
    name = 'app_course_test'
