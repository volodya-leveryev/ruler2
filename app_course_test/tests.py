from django.test import TestCase

from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_course.models import Course
from app_course_test.models import Test


class TestStudentTestViews(TestCase):

    def setUp(self):
        user = mommy.prepare(User, username='user')
        user.set_password('password')
        user.save()

        self.course = mommy.make(Course, name='course')
        self.test = mommy.make(Test, course=self.course, name='test', user=user)

    def test_course_page(self):
        """ Студент на странице курса должен видеть названия тестов """
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course:student_page', args=[self.course.id]))
        self.assertContains(r, self.test.name)

    def test_test_page(self):
        """ Студент на странице теста должен видеть название теста """
        self.client.login(username='user', password='password')

        r = self.client.get(reverse('course_test:test_page', args=[self.test.id]))
        self.assertContains(r, self.test.name)
