from django.contrib import admin

from app_course_test.models import Test, TestQuestion


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    list_display = ('order', 'name', 'course')
    list_filter = ('course',)


@admin.register(TestQuestion)
class TestQuestionAdmin(admin.ModelAdmin):
    list_display = ('code', 'test')
    list_filter = ('test',)
