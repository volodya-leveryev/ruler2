from django import template
from django.forms import BoundField

register = template.Library()


@register.filter
def form_class(bound_field: BoundField, cls: str):
    classes = set(bound_field.field.widget.attrs.get('class', '').split())
    classes.add(cls)
    bound_field.field.widget.attrs['class'] = ' '.join(classes)
    return bound_field


@register.filter
def hidden(bound_field: BoundField):
    return form_class(bound_field, 'd-none')


@register.filter
def get_class(value):
    return value.__class__.__name__


@register.inclusion_tag('base/form_control.html')
def form_control(bound_field: BoundField, col: str = '') -> dict:
    try:
        attributes = bound_field.field.widget.attrs
    except AttributeError as e:
        print('Missing field: %s' % bound_field)
        raise e
    css_classes = ['form-control']
    if bound_field.errors:
        css_classes.append('is-invalid')
    if 'class' in attributes:
        css_classes.append(attributes['class'])
    attributes['class'] = ' '.join(css_classes)
    return {'col': col, 'field': bound_field, 'help': str(bound_field.help_text)}


@register.inclusion_tag('base/form_check.html')
def form_check(bound_field: BoundField, col: str = '') -> dict:
    attributes = bound_field.field.widget.attrs
    css_classes = ['form-check-input']
    if bound_field.errors:
        css_classes.append('is-invalid')
    if 'class' in attributes:
        css_classes.append(attributes['class'])
    attributes['class'] = ' '.join(css_classes)
    return {'col': col, 'field': bound_field, 'help': str(bound_field.help_text)}
