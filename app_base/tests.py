from typing import List

from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User


class TestViewsCommon(TestCase):

    def setUp(self):
        self.user = mommy.prepare(User, username='user')
        self.user.set_password('old_password')
        self.user.save()

        self.login_url = reverse('user:login')
        self.password_change_url = reverse('user:change_password')

    def test_user_can_change_password(self):
        r = self.client.get(self.password_change_url)
        self.assertRedirects(r, '%s?next=%s' % (self.login_url, self.password_change_url))

        self.client.login(username=self.user.username, password='old_password')

        r = self.client.post(self.password_change_url, {
            'old_password': 'wrong_password',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
        })
        user = User.objects.get(pk=self.user.pk)
        self.assertContains(r, 'пароль введен неправильно')
        self.assertTrue(user.check_password('old_password'))
        self.assertFalse(user.check_password('new_password'))

        r = self.client.post(self.password_change_url, {
            'old_password': 'old_password',
            'new_password1': 'new_password1',
            'new_password2': 'new_password2',
        })
        user = User.objects.get(pk=self.user.pk)
        self.assertContains(r, 'не совпадают')
        self.assertTrue(user.check_password('old_password'))
        self.assertFalse(user.check_password('password1'))
        self.assertFalse(user.check_password('password2'))

        r = self.client.post(self.password_change_url, {
            'old_password': 'old_password',
            'new_password1': 'new_password',
            'new_password2': 'new_password',
        })
        self.assertRedirects(r, reverse('base:index'))
        user = User.objects.get(pk=self.user.pk)
        self.assertFalse(user.check_password('old_password'))
        self.assertTrue(user.check_password('new_password'))


class TestViewsAdmin(TestCase):

    def check_unauthorized_user_cannot_access(self, user: User, url: str) -> None:
        login_url = reverse('user:login')
        self.client.login(username=user.username, password='password')
        r = self.client.get(url)
        self.assertRedirects(r, '%s?next=%s' % (login_url, url))

    def check_unauthorized_access(self, urls: List[str]) -> None:
        teacher = mommy.prepare(User, is_superuser=False, is_staff=True)
        teacher.set_password('password')
        teacher.save()

        student = mommy.prepare(User, is_superuser=False, is_staff=False)
        student.set_password('password')
        student.save()

        for user in [teacher, student]:
            for url in urls:
                self.check_unauthorized_user_cannot_access(user, url)


class TestViewsTeacher(TestCase):

    def check_unauthorized_user_cannot_access(self, user: User, url: str) -> None:
        login_url = reverse('user:login')
        self.client.login(username=user.username, password='password')
        r = self.client.get(url)
        self.assertRedirects(r, '%s?next=%s' % (login_url, url))

    def check_unauthorized_access(self, urls: List[str]) -> None:
        unauthorized_teacher = mommy.prepare(User, is_superuser=False, is_staff=True)
        unauthorized_teacher.set_password('password')
        unauthorized_teacher.save()

        student = mommy.prepare(User, is_superuser=False, is_staff=False)
        student.set_password('password')
        student.save()

        for user in [unauthorized_teacher, student]:
            for url in urls:
                self.check_unauthorized_user_cannot_access(user, url)
