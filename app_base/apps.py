from django.apps import AppConfig


class BaseConfig(AppConfig):
    name = 'app_base'
