from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.views import PasswordChangeView, LoginView, LogoutView
from django.forms import ModelForm, modelformset_factory, TextInput
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import FormView
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from app_user.models import User
from app_student_status.models import StudentStatus


class UserForm(ModelForm):
    class Meta:
        model = User
        fields = [
            'username', 'email', 'gender',
            'last_name', 'first_name', 'second_name',
            'is_active', 'is_staff', 'is_superuser',
        ]

    def __init__(self, **kwargs):
        # data['formset'] = StatusFormSet()
        super().__init__(**kwargs)

    def clean(self):
        return super().clean()

    def is_valid(self):
        return super().is_valid()

    def save(self, commit=True):
        return super().save(commit)


StatusFormSet = modelformset_factory(
    StudentStatus,
    fields='__all__',
    can_delete=True,
    can_order=True,
    widgets={
        'comment': TextInput
    }
)


class LoginPage(LoginView):
    template_name = 'user/login.html'


class LogoutPage(LogoutView):
    template_name = 'user/logged_out.html'


@method_decorator(login_required, name='dispatch')
class PasswordChangePage(PasswordChangeView):
    template_name = 'user/password_change_form.html'
    success_url = reverse_lazy('base:index')


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class SetPasswordPage(FormView):
    form_class = SetPasswordForm
    success_url = reverse_lazy('user:list')
    template_name = 'user/set_password_form.html'

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['user'] = get_object_or_404(User, pk=self.kwargs['user_id'])
        return kwargs


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class List(ListView):
    model = User
    template_name = 'user/list.html'


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Create(CreateView):
    model = User
    fields = [
        'username', 'email', 'gender',
        'last_name', 'first_name', 'second_name',
        'is_active', 'is_staff', 'is_superuser'
    ]
    success_url = reverse_lazy('user:list')
    template_name = 'user/form.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['formset'] = StatusFormSet()
        return context

    def form_valid(self, form):
        formset_statuses = StatusFormSet(self.request.POST)
        if formset_statuses.is_valid():
            formset_statuses.save()
        return super().form_valid(form)


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Update(UpdateView):
    form_class = UserForm
    model = User
    success_url = reverse_lazy('user:list')
    template_name = 'user/form.html'


@method_decorator(user_passes_test(lambda u: u.is_superuser), name='dispatch')
class Delete(DeleteView):
    model = User
    success_url = reverse_lazy('user:list')
    template_name = 'user/confirm_delete.html'
