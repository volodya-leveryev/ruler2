from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    MALE = 'M'
    FEMALE = 'F'
    GENDERS = [
        (MALE, 'мужской'),
        (FEMALE, 'женский'),
    ]

    second_name = models.CharField('отчество', max_length=30, blank=True)
    birthday = models.DateField('дата рождения', blank=True, null=True)
    gender = models.CharField('пол', max_length=1, choices=GENDERS, blank=True)

    class Meta:
        verbose_name = 'пользователь'
        verbose_name_plural = 'пользователи'

    def __str__(self):
        result = self.username
        if self.last_name:
            result = self.last_name
            if self.first_name:
                result += ' ' + self.first_name
                if self.second_name:
                    result += ' ' + self.second_name
        return result


User._meta.get_field('is_staff').verbose_name = 'преподаватель'
User._meta.get_field('is_superuser').verbose_name = 'администратор'
