from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_base.tests import TestViewsAdmin
from app_user.models import User


class TestUserModel(TestCase):

    def test_str(self):
        obj = User(username='user')
        self.assertEqual(obj.username, str(obj))

        obj.last_name = 'last'
        self.assertEqual(obj.last_name, str(obj))

        obj.first_name = 'first'
        self.assertEqual('%s %s' % (obj.last_name, obj.first_name), str(obj))

        obj.second_name = 'second'
        self.assertEqual('%s %s %s' % (obj.last_name, obj.first_name, obj.second_name), str(obj))

    def test_username_is_unique(self):
        """ Имя пользователя должно быть уникально """
        mommy.make(User, username='name')
        self.assertRaises(IntegrityError, mommy.make, User, username='name')


class TestAuthenticationViews(TestCase):

    def setUp(self):
        user = mommy.prepare(User, username='user')
        user.set_password('password')
        user.save()

    def test_login_page(self):
        index_url = reverse('base:index')
        login_url = '%s?next=%s' % (reverse('user:login'), index_url)

        # Неавторизованный пользователь
        r = self.client.get(index_url)
        self.assertRedirects(r, login_url)

        # Страница входа
        r = self.client.get(login_url)
        self.assertTemplateUsed(r, 'user/login.html')

        # Неправильный логин
        r = self.client.post(login_url, {'username': 'wrong', 'password': 'password'})
        self.assertContains(r, 'введите правильные имя пользователя и пароль', status_code=200)

        # Неправильный пароль
        r = self.client.post(login_url, {'username': 'user', 'password': 'wrong'})
        self.assertContains(r, 'введите правильные имя пользователя и пароль', status_code=200)

        # Правильный логин и пароль
        r = self.client.post(login_url, {'username': 'user', 'password': 'password'})
        self.assertRedirects(r, index_url)


class TestAdminUserViews(TestViewsAdmin):

    def setUp(self):
        self.admin = mommy.prepare(User, username='admin', is_superuser=True, is_staff=False)
        self.admin.set_password('password')
        self.admin.save()

        self.student = mommy.prepare(User, username='student', is_superuser=False, is_staff=False)
        self.student.set_password('password')
        self.student.save()

        self.user_list_url = reverse('user:list')
        self.user_edit_url = reverse('user:edit', args=[self.student.id])
        self.password_url = reverse('user:set_password', args=[self.student.id])

        self.password_data1 = {'new_password1': '12345678', 'new_password2': '12345678'}
        self.password_data2 = {'new_password1': '4w2K9QHD', 'new_password2': '4w2K9QHD'}

    def test_unauthorized_access(self):
        self.check_unauthorized_access([
            self.user_edit_url,
            self.user_list_url,
            self.password_url,
        ])

    def test_admin_can_view_user_list(self):
        self.client.login(username=self.admin.username, password='password')

        r = self.client.get(self.user_list_url)
        self.assertContains(r, str(self.admin))

    def test_admin_can_edit_user(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.user_edit_url)
        self.assertContains(r, str(self.student))

        r = self.client.post(self.user_edit_url, {
            'username': 'new_user',
        })
        self.assertRedirects(r, self.user_list_url)

    def test_admin_can_view_user_set_password(self):
        self.client.login(username=self.admin.username, password='password')

        r = self.client.get(self.password_url)
        self.assertContains(r, str(self.student))

        r = self.client.post(self.password_url, self.password_data1)
        self.assertContains(r, 'пароль состоит только из цифр')

        r = self.client.post(self.password_url, self.password_data2)
        self.assertRedirects(r, reverse('user:list'))
