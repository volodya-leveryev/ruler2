coverage==4.5.3
Django==2.2.1
django-debug-toolbar==1.11
Markdown==3.1.1
model-mommy==1.6.0
selenium==3.141.0
