from django.contrib.auth.decorators import user_passes_test
from django.forms import ModelForm
from django.shortcuts import render, get_object_or_404, redirect

from app_semester.models import Semester


class SemesterForm(ModelForm):
    class Meta:
        model = Semester
        fields = '__all__'


@user_passes_test(lambda u: u.is_superuser)
def semester_list(request):
    return render(request, 'semester/list.html', {'semester_list': Semester.objects.all()})


@user_passes_test(lambda u: u.is_superuser)
def semester_form(request, semester_pk=None):
    semester = get_object_or_404(Semester, pk=semester_pk) if semester_pk else None
    if request.method == 'POST':
        form = SemesterForm(request.POST, instance=semester)
        if form.is_valid():
            form.save()
            return redirect('semester::list')
    else:
        form = SemesterForm(instance=semester)
    context = {'form': form}
    if semester_pk:
        context['students'] = semester.get_students()
    return render(request, 'semester/form.html', context)


@user_passes_test(lambda u: u.is_superuser)
def semester_delete(request, semester_pk):
    semester = get_object_or_404(Semester, pk=semester_pk)
    if request.method == 'POST':
        semester.delete()
        return redirect('semester::list')
    return render(request, 'semester/confirm_delete.html', {'app_semester': semester})
