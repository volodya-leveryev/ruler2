from datetime import datetime

from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

from app_user.models import User
from app_base.tests import TestViewsAdmin
from app_semester.models import Semester
from app_student_group.models import StudentGroup
from app_student_status.models import StudentStatus


class TestSemesterModel(TestCase):

    def test_str(self):
        obj = mommy.make(Semester, year=2018, semester=Semester.SPRING)
        self.assertEqual('%s: 2017 – 2018 уч.г., весенний семестр' % obj.student_group, str(obj))

        obj = mommy.make(Semester, year=2018, semester=Semester.AUTUMN)
        self.assertEqual('%s: 2018 – 2019 уч.г., осенний семестр' % obj.student_group, str(obj))

    def test_get_students(self):
        group1 = mommy.make(StudentGroup)
        group2 = mommy.make(StudentGroup)

        moment1 = datetime(2017, 1, 1)
        moment2 = datetime(2018, 1, 1)
        moment3 = datetime(2019, 1, 1)

        # Нормальный студент
        student1 = mommy.make(User)
        mommy.make(StudentStatus, student=student1, moment=moment1, student_group=group1, status='E')

        # Отчисленный студент
        student2 = mommy.make(User)
        mommy.make(StudentStatus, student=student2, moment=moment1, student_group=group1, status='E')
        mommy.make(StudentStatus, student=student2, moment=moment2, student_group=group1, status='X')

        # Студент зачисленный позднее
        student3 = mommy.make(User)
        mommy.make(StudentStatus, student=student3, moment=moment3, student_group=group1, status='E')

        # Студент из другой группы
        student4 = mommy.make(User)
        mommy.make(StudentStatus, student=student4, moment=moment1, student_group=group2, status='E')

        # В списке зачисленных должен быть только первый студент
        obj = mommy.make(Semester, student_group=group1, semester='A', year=2018, begin_study=moment2.date())
        result = obj.get_students()
        self.assertEqual(1, len(result))
        self.assertEqual(student1, result[0])


class TestAdminSemesterViews(TestViewsAdmin):

    def setUp(self):
        self.admin = mommy.prepare(User, username='admin', is_superuser=True, is_staff=False)
        self.admin.set_password('password')
        self.admin.save()

        semester = mommy.make(Semester)

        self.semester_create_url = reverse('semester:create')
        self.semester_edit_url = reverse('semester:edit', args=[semester.id])

    def test_unauthorized_access(self):
        self.check_unauthorized_access([self.semester_create_url, self.semester_edit_url])

    def test_admin_can_view_semester_edit(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.semester_edit_url)
        self.assertContains(r, 'Редактирование семестра')

    def test_admin_can_view_semester_create(self):
        self.client.login(username=self.admin.username, password='password')
        r = self.client.get(self.semester_create_url)
        self.assertContains(r, 'Создание семестра')
