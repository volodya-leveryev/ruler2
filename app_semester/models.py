from django.db import models
from django.db.models.query import RawQuerySet


class Semester(models.Model):
    AUTUMN = 'A'
    SPRING = 'S'
    SEMESTER = (
        (AUTUMN, 'осенний семестр'),
        (SPRING, 'весенний семестр')
    )

    student_group = models.ForeignKey('app_student_group.StudentGroup', on_delete=models.CASCADE,
                                      verbose_name='учебная группа')
    year = models.IntegerField('календарный год')
    semester = models.CharField('семестр', max_length=1, choices=SEMESTER)
    begin_study = models.DateField('начало обучения')
    end_study = models.DateField('окончание обучения')
    begin_exams = models.DateField('начало сессии')
    end_exams = models.DateField('окончание сессии')

    class Meta:
        verbose_name = 'семестр'
        verbose_name_plural = 'семестры'

    def __str__(self):
        return '%s: %s, %s' % (self.student_group.name, self.get_year(), self.get_semester_display())

    def get_year(self) -> str:
        """Представление учебного года"""
        if self.semester == Semester.AUTUMN:
            begin = self.year
            end = self.year + 1
        else:
            begin = self.year - 1
            end = self.year
        return '%d – %d уч.г.' % (begin, end)

    def get_students(self) -> RawQuerySet:
        """Студенты, которые учатся в этом семестре в этой группе"""
        return self.student_group.get_students(self.begin_study)
