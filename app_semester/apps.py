from django.apps import AppConfig


class SemesterConfig(AppConfig):
    name = 'app_semester'
