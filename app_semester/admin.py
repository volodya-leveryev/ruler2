from django.contrib import admin

from app_semester.models import Semester

admin.site.register(Semester)
